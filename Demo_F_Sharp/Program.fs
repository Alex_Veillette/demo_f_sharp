﻿open System

(*VARIABLES GLOBALES*)
let mutable mutable_firstname = "Vide"
let mutable mutable_lastname = "Vide"

(*FONCTIONS RÉUTILISÉES*)
let show_bar() =
    printfn "==============================\n"

(*ENTRÉES UTILISATEUR*)
let enter_firstname() =
    printf "Entrer votre prénom: "
    let firstname = Console.ReadLine()
    let old_mutable_firstname = mutable_firstname
    mutable_firstname <- firstname
    printfn "(variable mutable): %s devient %s" old_mutable_firstname mutable_firstname

let enter_lastname() =
    printf "Entrer votre nom: "
    let lastname = Console.ReadLine()
    let old_mutable_lastname = mutable_lastname
    mutable_lastname <- lastname
    printfn "(variable mutable): %s devient %s" old_mutable_lastname mutable_lastname

(*OPÉRATIONS*)
let get_sum(int_01: int, int_02: int): int = 
    int_01 + int_02

let get_dif(int_01: int, int_02: int): int = 
    int_01 - int_02

let get_mult(int_01: int, int_02: int): int = 
    int_01 * int_02

let get_div(int_01: int, int_02: int): int = 
    int_01 / int_02

let get_mod(int_01: int, int_02: int): int =
    int_01 % int_02

let get_pow(float_01: float, float_02: float): float =
    float_01 ** float_02

(*FONCTIONS RÉCURSIVES*)
let rec factorial int_01 =
    if int_01 < 1 then 1
    else int_01 * factorial(int_01 - 1)

(*FONCTIONS AFFICHAGE*)
let show_fullname() =
    let fullname = mutable_firstname + " " + mutable_lastname
    printfn "Nom complet: %s" fullname
    show_bar()

let show_references() =
    let int_before_change = ref 100
    printfn "Int ref #1 (avant): %i" !int_before_change
    int_before_change := 200
    printfn "Int ref #2 (après): %i" !int_before_change
    show_bar()

let show_ints() =
    let my_int_01 = 100
    printfn "Int #1 (défaut): %i" my_int_01
    show_bar()

let show_floats() =
    let my_float_01 = 123.1234567890
    let my_precise_float_01 = 123.1234567890M

    printfn "Float #1 (défaut): %f" my_float_01
    printfn "Float #2 (2 décimales): %.2f" my_float_01
    printfn "Float #3 (précis): %M" my_precise_float_01
    show_bar()

let show_strings() =
    let my_string_01 = "ABCDE"
    let my_string_02 = "FGHIJ"
    let dynamic_padding = 8

    printfn "String #1 (défaut): %s" my_string_01
    printfn "String #2 (composée): %s + %s" my_string_01 my_string_02
    printfn "String #3 (padding `<--`): %-10s" my_string_01
    printfn "String #4 (padding `-->`): %10s" my_string_01
    printfn "String #5 (padding `<-- -->`): %-10s %10s" my_string_01 my_string_02
    printfn "String #6 (padding `--> <--`): %10s %-10s" my_string_01 my_string_02
    printfn "String #7 (Dpadding `-->`): %*s" dynamic_padding my_string_01
    show_bar()

let show_bools() =
    let my_bool_01 = false
    let my_bool_02 = true

    printfn "Bool #1: %b" my_bool_01
    printfn "Bool #2: %b" my_bool_02
    show_bar()

let show_lists() =
    let my_list_01 = [1; 2; 3; 4; 5; 6; 7; 8; 9]

    printfn "Liste #1: %A" my_list_01
    show_bar()

let show_operations() =
    let my_int_01 = 10
    let my_int_02 = 5
    let my_int_03 = -5
    let my_float_01 = 5.0
    let my_float_02 = 2.0
    let my_float_03 = 100.0
    let my_float_04 = 2.5
    let my_sum_01 = get_sum(my_int_01, my_int_02)
    let my_dif_01 = get_dif(my_int_01, my_int_02)
    let my_mult_01 = get_mult(my_int_01, my_int_02)
    let my_div_01 = get_div(my_int_01, my_int_02)
    let my_mod_01 = get_mod(my_int_01, 2)
    let my_pow_01 = get_pow(my_float_01, my_float_02)

    printfn "Addition #1: %i + %i = %i" my_int_01 my_int_02 my_sum_01
    printfn "Soustraction #1: %i - %i = %i" my_int_01 my_int_02 my_dif_01
    printfn "Multiplication #1: %i * %i = %i" my_int_01 my_int_02 my_mult_01
    printfn "Division #1: %i / %i = %i" my_int_01 my_int_02 my_div_01
    printfn "Modulo #1: %i %% 2 = %i" my_int_01 my_mod_01
    printfn "Exposant #1: %f ^ %f = %f" my_float_01 my_float_02 my_pow_01
    show_bar()

    printfn "Absolu de %i = %i" my_int_03 (abs my_int_03)
    printfn "Racine carrée de %f = %f" my_float_03 (sqrt my_float_03)
    printfn "Arrondit hausse de %f = %f" my_float_04 (ceil my_float_04)
    printfn "Arrondit baisse de %f = %f" my_float_04 (floor my_float_04)
    printfn "Logarithme de %f = %f" my_float_04 (log my_float_04)
    printfn "Logarithme 10 de %f = %f" my_float_03 (log10 my_float_03)
    show_bar()

let show_recursive_functions() =
    printfn "Fonction récursive #1 (factorielle 6): %i" (factorial 6)
    show_bar()          

let show_lambda_expressions() =
    let my_list_01 = [1; 2; 3; 4; 5; 6; 7; 8; 9]
    let my_list_01_mod = List.map(fun int_01 -> int_01 + 100) my_list_01

    printfn "Lambda liste #1 (défaut): %A" my_list_01
    printfn "Lambda liste #2 (défaut + 100): %A" my_list_01_mod
    show_bar()

let show_pipeline_operators() =
    [1; 2; 3; 4; 5]
    |> List.filter(fun int_01 -> (int_01 % 2) = 0)
    |> printfn "Opérateur pipeline #1 (forward + pairs): %A"

    [1; 2; 3; 4; 5]
    |> List.filter(fun int_01 -> (int_01 % 2) = 0)
    |> List.map(fun int_02 -> int_02 + 100)
    |> printfn "Opérateur pipeline #2 (forward + pairs + 100): %A"

    printfn "Opérateur pipeline #3 (backward): %A"
    <| [1; 2; 3; 4; 5]

    let add int_01 = int_01 + 10
    let mult int_02 = int_02 * 10
    let add_mult = add >> mult
    let mult_add = add << mult
    printfn "Opérateur composition #1 (forward)(addition >> multiplication): %i" (add_mult 5)
    printfn "Opérateur composition #2 (backward)(addition << multiplication): %i" (mult_add 5)
    show_bar()

let show_data_modification() =
    let my_int_01 = 5
    printfn "Type de variable de %i : %A" my_int_01 (my_int_01.GetType())
    printfn "Conversion de INT en FLOAT de %i : %.2f" my_int_01 (float my_int_01)

(*ENTRÉES UTILISATEUR APPEL*)
enter_firstname()
enter_lastname()

(*FONCTIONS AFFICHAGE APPEL*)
show_fullname()
show_references()
show_ints()
show_floats()
show_strings()
show_bools()
show_lists()
show_operations() 
show_recursive_functions()
show_lambda_expressions()
show_pipeline_operators()
show_data_modification()

Console.ReadKey() |> ignore